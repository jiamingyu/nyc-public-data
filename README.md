# Requirement review and comments:

## Intial NYC public data provided in requirement document is deprecated

I tried both provided api and seems it has be deprecated and NOT accessible.

### What I worrking is to point to discovered API:
A quick research on how to get api data and query by api doc turns out to be here as example:
For school list: 
https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=2&$offset=10

For SAT score:
https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=01M292


# architect

This is a quick dev from master/detail template. Lots of code cleaning is not complete. Here is just some high level thing I try to address in this drill:

- Full list of schools will be achieved by keep scrolling. 
- Recycler view working with RxJava based schedulers to drive Retrofit loading more.
- ViewModel for activity/fragment to manage lifecycle change caused memory leak isssue. Disposable being taken care of on ViewModel clearing.
- LiveData stay within ViewModel and being observed at controller level to notify controller how to present data. Controllers don't need to have knowledge on how data access happened and worry free for memory leak and ANR from this perspective.
- Some android Instrumentation tests kick off.


# What can do better given the prevision for further development: 

- network retry strategy and util helper class def
- progress bar indicating on detail screen in case network loading take longer
- Search criteria and search filters to shorten interested school
- Convert multi activities to multi fragment based one, through nav compnents
- DiffCallBack help improve performance by help identifying updated holder, instead of calling adapter to update full data set
- Mater/detail is not perfect for this scenario & other design could drive to better outcome.


