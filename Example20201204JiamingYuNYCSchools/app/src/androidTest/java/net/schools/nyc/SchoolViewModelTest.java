package net.schools.nyc;

import androidx.lifecycle.ViewModelProviders;
import androidx.test.rule.ActivityTestRule;

import net.schools.nyc.controller.SchoolsActivity;
import net.schools.nyc.service.SchoolRetrofit;
import net.schools.nyc.viewmodel.SchoolViewModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static androidx.test.InstrumentationRegistry.getContext;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

@RunWith(JUnit4.class)
public class SchoolViewModelTest {
    @Rule
    public ActivityTestRule<SchoolsActivity> activityScenarioRule =
            new ActivityTestRule<SchoolsActivity>(SchoolsActivity.class, true, false);
    private MockWebServer mockedWebServer;

    // mocking response
    private final Dispatcher dispatcher = new Dispatcher() {
        Map<String, String> reqs2Resps = new HashMap();

        @Override
        public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
            reqs2Resps.put("",
                    ""
            );


            if (request.getPath().equals("/resource/s3k6-pzi2.json?$limit=1")) {
                try {
                return new MockResponse().setResponseCode(200).setBody(
                        FileReaderUtil.getStringFromFile(getContext(),"schoolResponse.json")
                );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return new MockResponse().setResponseCode(404);
        }
    };

    @Before
    public void setUp() throws Exception {
        mockedWebServer = new MockWebServer();
        mockedWebServer.setDispatcher(dispatcher);
        mockedWebServer.start();
        SchoolRetrofit.BASE_URL = mockedWebServer.url("/").toString();
    }

    @After
    public void tearDown() throws Exception {
        mockedWebServer.shutdown();
    }

    @Test
    public void test_api_response() {
        final SchoolViewModel schoolViewModel = ViewModelProviders.of(activityScenarioRule.getActivity()).get(SchoolViewModel.class);
        schoolViewModel.retrieveSchools();
        await().atMost(5, SECONDS).until(() -> schoolViewModel.getSchools().getValue() != null);

        assert(!schoolViewModel.getSchools().getValue().isEmpty());
        assert(schoolViewModel.getSchools().getValue().size() == 1);

    }

}
