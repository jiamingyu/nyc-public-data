package net.schools.nyc.controller;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import net.schools.nyc.R;
import net.schools.nyc.viewmodel.SchoolViewModel;


public class SchoolsActivity extends AppCompatActivity {

    private boolean mTwoPane;
    private SchoolViewModel mViewModel;
    private SchoolsRecyclerViewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(SchoolViewModel.class);
        mAdapter = new SchoolsRecyclerViewAdapter(mViewModel,this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.item_list);
        recyclerView.setItemAnimator(null);
        /** RecylerView itself has a bug when handling data update which will lead to crash. This is the work around */

        assert recyclerView != null;
        recyclerView.setAdapter(mAdapter);

        // Observe Livedata updating, define how adapter will update holders efficiently through Diff tool
        mViewModel.getSchools().observe(this, schoolInfos -> mAdapter.notifyDataSetChanged());
        mViewModel.observableErrorSatRec.observe(this, someThrowable -> {
            Snackbar.make(findViewById(R.id.item_list), getString(R.string.network_error_msg), Snackbar.LENGTH_LONG).setAction("Action", null).show();
        });

    }

}
