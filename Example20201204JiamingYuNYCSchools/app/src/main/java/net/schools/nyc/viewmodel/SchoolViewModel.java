package net.schools.nyc.viewmodel;


import androidx.lifecycle.MutableLiveData;

import net.schools.nyc.entity.SchoolInfo;
import net.schools.nyc.service.SchoolRetrofit;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class SchoolViewModel extends BaseViewModel<SchoolInfo> {

    private MutableLiveData<List<SchoolInfo>> schools = new MutableLiveData();
    private int offsetFactor = 0;
    public static final int PAGE_SIZE = 35;

    public MutableLiveData<List<SchoolInfo>> getSchools() {
        return schools;
    }

    public void retrieveSchools () {
        Observable<List<SchoolInfo>> schools = SchoolRetrofit.API.getList(PAGE_SIZE, PAGE_SIZE * offsetFactor);
        mDisposable = schools.subscribeOn(Schedulers.io()).observeOn(Schedulers.computation())
                .subscribe(this::handleUpdate, this::handleError);
    }

    @Override
    protected void handleUpdate(List<SchoolInfo> schIos) {
        List a = schools.getValue();
        if (a == null) {
            a = new ArrayList();
        }
        a.addAll(schIos);

        schools.postValue(a);
        offsetFactor += 1;
    }
}
