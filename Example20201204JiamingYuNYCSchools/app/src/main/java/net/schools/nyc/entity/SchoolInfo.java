package net.schools.nyc.entity;

public class SchoolInfo {
    private String dbn;
    private String school_name;
    private String boro;
    private String neighborhood;
    private String shared_space;
    private String campus_name;

    private String city;
    private String zip;
    private String state_code;
    private String latitude;
    private String longitude;
    private String community_board;
    private String council_district;
    private String census_tract;
    private String bin;
    private String bbl;
    private String nta;
    private String borough;


    // Getter Methods

    public String getDbn() {
        return dbn;
    }

    public String getSchool_name() {
        return school_name;
    }

    public String getBoro() {
        return boro;
    }


    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public String getState_code() {
        return state_code;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getCommunity_board() {
        return community_board;
    }

    public String getCouncil_district() {
        return council_district;
    }

    public String getCensus_tract() {
        return census_tract;
    }

    public String getBin() {
        return bin;
    }

    public String getBbl() {
        return bbl;
    }

    public String getNta() {
        return nta;
    }

    public String getBorough() {
        return borough;
    }

    // Setter Methods

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public void setBoro(String boro) {
        this.boro = boro;
    }


    public void setCity(String city) {
        this.city = city;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setCommunity_board(String community_board) {
        this.community_board = community_board;
    }

    public void setCouncil_district(String council_district) {
        this.council_district = council_district;
    }

    public void setCensus_tract(String census_tract) {
        this.census_tract = census_tract;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public void setBbl(String bbl) {
        this.bbl = bbl;
    }

    public void setNta(String nta) {
        this.nta = nta;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }
}
