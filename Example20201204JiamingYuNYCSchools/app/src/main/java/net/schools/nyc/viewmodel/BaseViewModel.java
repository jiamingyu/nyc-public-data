package net.schools.nyc.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import net.schools.nyc.entity.SchoolSatRec;

import java.util.List;

import io.reactivex.disposables.Disposable;

public abstract class BaseViewModel<T>  extends ViewModel {
    protected Disposable mDisposable;
    public final MutableLiveData<Boolean> observableErrorSatRec = new MutableLiveData();

    public Disposable getDisposable() {
        return mDisposable;
    }


    protected void handleError(Throwable throwable) {
        observableErrorSatRec.postValue(true);
    }

    protected void onCleared() {
        super.onCleared();
        // prevent memory leak on viewmodel cleared for rx disposable
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }

    abstract protected void handleUpdate(List<T> schools);
}
