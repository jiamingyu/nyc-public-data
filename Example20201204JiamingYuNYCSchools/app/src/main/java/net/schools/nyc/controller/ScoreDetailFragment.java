package net.schools.nyc.controller;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;

import net.schools.nyc.R;
import net.schools.nyc.databinding.ItemDetailBinding;
import net.schools.nyc.viewmodel.SchoolSatViewModel;


public class ScoreDetailFragment extends Fragment {

    public static final String ARG_DBN_ID = "DBN";

    private ItemDetailBinding mBinding;
    private SchoolSatViewModel mViewModel;

    public ScoreDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(SchoolSatViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);
        mBinding = ItemDetailBinding.inflate(inflater, container, false);

        mViewModel.getSatRecord().observe(this, schoolSatRec -> {
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(schoolSatRec.getSchool_name());
            }

            mBinding.schName.setText(schoolSatRec.getSchool_name());
            mBinding.mathSat.setText( schoolSatRec.getSat_math_avg_score());
            mBinding.readSat.setText( schoolSatRec.getSat_critical_reading_avg_score());
            mBinding.writeSat.setText( schoolSatRec.getSat_writing_avg_score());
        });


        mViewModel.observableErrorSatRec.observe(this, someThrowable -> {
            Snackbar.make(mBinding.getRoot(), getString(R.string.network_error_msg), Snackbar.LENGTH_LONG).setAction("Action", null).show();
        });

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments().containsKey(ARG_DBN_ID)) {
            mViewModel.retrieveSchoolSatRecord(getArguments().getString(ARG_DBN_ID));
        }
    }
}
