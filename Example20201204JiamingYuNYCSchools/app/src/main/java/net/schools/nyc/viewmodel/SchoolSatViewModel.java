package net.schools.nyc.viewmodel;

import androidx.lifecycle.MutableLiveData;

import net.schools.nyc.entity.SchoolSatRec;
import net.schools.nyc.service.SchoolRetrofit;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class SchoolSatViewModel extends BaseViewModel<SchoolSatRec> {
    private MutableLiveData<SchoolSatRec> satRecord = new MutableLiveData(); //TODO: public final?

    public void retrieveSchoolSatRecord(String dbnId) {
        Observable<List<SchoolSatRec>> srec =  SchoolRetrofit.API.schoolAcademicData(dbnId);
        srec.subscribeOn(Schedulers.io()).observeOn(Schedulers.computation())
                .subscribe(this::handleUpdate, this::handleError);
    }

    @Override
    protected void handleUpdate(List<SchoolSatRec> schools) {
        if (!schools.isEmpty()) {
            satRecord.postValue(schools.get(0));
        }
    }

    public MutableLiveData<SchoolSatRec> getSatRecord() {
        return satRecord;
    }
}
