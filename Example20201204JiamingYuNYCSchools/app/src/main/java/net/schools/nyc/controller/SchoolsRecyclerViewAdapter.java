package net.schools.nyc.controller;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import net.schools.nyc.R;
import net.schools.nyc.databinding.CardLoadingBinding;
import net.schools.nyc.entity.SchoolInfo;
import net.schools.nyc.viewmodel.SchoolViewModel;

public class SchoolsRecyclerViewAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected static final int CARD_HOLDER_VIEW = 10;
    protected static final int LOAD_HOLDER_VIEW = 20;

    private SchoolViewModel viewModel;
    private final SchoolsActivity mParentActivity;

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            Intent intent = new Intent(context, SchoolScoresActivity.class);
            intent.putExtra(ScoreDetailFragment.ARG_DBN_ID, (String)view.getTag(R.string.dbn_id));
            context.startActivity(intent);
        }
    };
    SchoolsRecyclerViewAdapter(SchoolViewModel viewModel, SchoolsActivity parent
                                  ) {
        this.viewModel = viewModel;
        mParentActivity = parent;
    }

    @Override
    public int getItemViewType(int position) {
        // For initial loading trigger, when recyclerView empty, use this to define loading type and trigger api call
        if (getItemCount() == 1) {
            return LOAD_HOLDER_VIEW;
        }

        if (position == getItemCount()-1) {
            return LOAD_HOLDER_VIEW;
        } else {
            return CARD_HOLDER_VIEW;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case LOAD_HOLDER_VIEW: return new LoadingViewHolder(CardLoadingBinding.inflate(LayoutInflater.from(mParentActivity), parent, false));
            default: {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_list_content, parent, false);
                return (RecyclerView.ViewHolder)(new CardViewHolder(view));
            }
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CardViewHolder && viewModel.getSchools().getValue() != null) {
            SchoolInfo schoolInfo = viewModel.getSchools().getValue().get(position);

            CardViewHolder cardViewHolderholder = (CardViewHolder)holder;
            cardViewHolderholder.mIdView.setText(schoolInfo.getDbn());
            cardViewHolderholder.mContentView.setText(schoolInfo.getSchool_name());

            cardViewHolderholder.itemView.setTag(R.string.dbn_id,schoolInfo.getDbn());
            cardViewHolderholder.itemView.setOnClickListener(mOnClickListener);
        } else {
            ((LoadingViewHolder) holder).bind();
        }
    }

    @Override
    public int getItemCount() {
        if ( viewModel.getSchools().getValue()== null || viewModel.getSchools().getValue().isEmpty()) {
            return 1;
        } else {
            return viewModel.getSchools().getValue().size() + 1;
        }
    }

    class CardViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;
        final TextView mContentView;

        CardViewHolder(View view) {
            super(view);
            mIdView = (TextView) view.findViewById(R.id.id_text);
            mContentView = (TextView) view.findViewById(R.id.content);
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder{
        private CardLoadingBinding binding ;

        public LoadingViewHolder(@NonNull CardLoadingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind() {
            binding.spinner.setVisibility(View.VISIBLE);
            viewModel.retrieveSchools();
            binding.executePendingBindings();
        }
    }

    class DiffCallBack extends DiffUtil.ItemCallback<SchoolInfo> {

        @Override
        public boolean areItemsTheSame(@NonNull SchoolInfo oldItem, @NonNull SchoolInfo newItem) {
            return oldItem.getDbn() == newItem.getDbn();
        }

        @Override
        public boolean areContentsTheSame(@NonNull SchoolInfo oldItem, @NonNull SchoolInfo newItem) {
            return oldItem.getSchool_name().equalsIgnoreCase(newItem.getSchool_name());
        }
    }

}
