package net.schools.nyc.service;


import com.google.gson.JsonArray;

import net.schools.nyc.entity.SchoolInfo;
import net.schools.nyc.entity.SchoolSatRec;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolApis {
    @GET("resource/s3k6-pzi2.json?")
    public Observable<List<SchoolInfo>> getList(@Query("$limit") int limit, @Query("$offset") int offset);

    @GET("resource/f9bf-2cp4.json")
    public Observable<List<SchoolSatRec>> schoolAcademicData(@Query("dbn") String dbnId);
}
